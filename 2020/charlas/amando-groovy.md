# Título de la propuesta

Lo que te puede enamorar de groovy (si no lo ha hecho ya).

## Formato de la propuesta

Indicar uno de estos:

* [ ] Charla (25 minutos)
* [x] Charla relámpago (10 minutos)

## Descripción

En esta charla se explicarán algunos conceptos prácticos de groovy, que puedes probar hoy mismo
y que te llevarán a comprender la potencia de este lenguaje. 

En ella pretendo explicar de forma descriptiva los siguientes conceptos:

- Gstring: la libertad del String.
- Each: haciendo simple recorrer una lista.
- Find y findAll: 
- List y maps: nunca ha sido tan fácil crear estas estructuras
- Descargar un xml o json remoto y navegarlo 
- Http Post con http builder
(Y alguna más q me dejo)


## Público objetivo

Para gente que aún no conoce Groovy o que son sus primeros pasos. 

## Ponente(s)


Mi nombre es Miguel Rueda y trabajo con desarrollador desde el 2010. Actualmente estoy cooperando 
en la creación de un libro tutorial http://groovy-lang.gitlab.io/101-scripts/ en el que estamos creando un documento que
recoja los conocimientos que tenemos acerca de la programación en groovy 

He dado una única charla en el Greach 2018 en Madrid

### Contacto(s)

* Miguel Rueda García: miguel.rueda.garcia@gmail.com

## Comentarios

El formato de la charla será la explicación de unos breves ejemplos donde se vea rápidamente lo expuesto durante la misma.

## Condiciones

* [x] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] Al menos una persona entre los que la proponen estará presente el día programado para la charla.